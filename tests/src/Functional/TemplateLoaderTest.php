<?php

namespace Drupal\Tests\snippet_manager\Functional;

/**
 * Template loader test.
 *
 * @group snippet_manager
 */
class TemplateLoaderTest extends TestBase {

  /**
   * Tests template loader context.
   */
  public function testTemplateLoader() {
    $edit = [
      'id' => 'base',
      'label' => 'Base',
      'page[status]' => TRUE,
      'page[path]' => '/foo',
    ];
    $this->drupalPostForm('admin/structure/snippet/add', $edit, 'Save');

    $edit = [
      'template[value]' => sprintf('<div class="snippet-base">{%% include "@snippet/%s" %%}</div>', $this->snippetId),
      'template[format]' => 'snippet_manager_test_basic_format',
    ];

    $this->drupalPostForm('admin/structure/snippet/base/edit/template', $edit, 'Save');

    $this->drupalGet('admin/structure/snippet/base');
    $this->assertXpath('//div[@class="snippet-base"]/div[@class="snippet-test" and .="9"]');

    // Disable snippet and check that it is not longer included.
    $this->drupalGet('admin/structure/snippet');
    $this->click(sprintf('//td//a[contains(@href, "admin/structure/snippet/%s/disable")]', $this->snippetId));

    \Drupal::service('twig')->invalidate();

    $this->drupalLogout();

    $this->drupalGet('/foo');
    $this->assertSession()->statusCodeEquals(500);
    $message = sprintf('Template &quot;@snippet/%s&quot; is not defined', $this->snippetId);
    $this->assertSession()->responseContains($message);
  }

}
