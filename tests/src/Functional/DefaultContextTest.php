<?php

namespace Drupal\Tests\snippet_manager\Functional;

use Drupal\Core\Url;

/**
 * Default context test.
 *
 * @group snippet_manager
 */
class DefaultContextTest extends TestBase {

  /**
   * Tests default context.
   */
  public function testDefaultContext() {
    $this->drupalGet('admin/structure/snippet/gamma');
    $prefix = '//ol[@class="default-context"]';
    $this->assertXpath($prefix . '/li[1][. = "classy"]');
    $this->assertXpath($prefix . '/li[2][. = "core/themes/classy"]');
    $this->assertXpath($prefix . sprintf('/li[3][. = "%s"]', base_path()));
    $this->assertXpath($prefix . sprintf('/li[4][. = "%s"]', Url::fromRoute('<front>')->toString()));
    $this->assertXpath($prefix . '/li[5][. = ""]');
    $this->assertXpath($prefix . '/li[6][. = ""]');
    $this->assertXpath($prefix . '/li[7][. = "1"]');
  }

}
