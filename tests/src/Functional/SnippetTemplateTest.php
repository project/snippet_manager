<?php

namespace Drupal\Tests\snippet_manager\Functional;

use Drupal\Component\Render\FormattableMarkup;

/**
 * Snippet template test.
 *
 * @group snippet_manager
 */
class SnippetTemplateTest extends TestBase {

  /**
   * Tests snippet template form.
   */
  public function testSnippetTemplate() {
    $this->drupalGet($this->snippetEditUrl . '/template');
    $this->assertPageTitle(new FormattableMarkup('Edit %label', ['%label' => $this->snippetLabel]));

    $this->assertXpath('//div[contains(@class, "form-item-template-value") and label[.="Template"] and //textarea[@name="template[value]"]]');
    $this->assertXpath('//div[contains(@class, "form-item-template-format")]/label[.="Text format"]/../select/option[@selected="selected" and .="Snippet manager test basic format"]');

    $this->assertXpath('//table[caption[.="Variables"]]/tbody//td[.="Variables are not configured yet."]');
    $this->assertXpath('//div[contains(@class, "form-actions")]/input[@value="Save"]');
    $this->assertXpath(sprintf('//div[contains(@class, "form-actions")]/a[contains(@href, "/%s/edit/variable/add") and .="Add variable"]', $this->snippetUrl));

    // Submit form and check form default values.
    $edit = [
      'template[value]' => '<div class="snippet-test">{{ 3 + 4 }}</div>',
      'template[format]' => 'snippet_manager_test_basic_format',
    ];
    $this->drupalPostForm($this->snippetEditUrl . '/template', $edit, 'Save');

    $this->assertStatusMessage(new FormattableMarkup('Snippet %label has been updated.', ['%label' => $this->snippetLabel]));

    $this->assertSession()->addressEquals($this->snippetEditUrl . '/template');

    $this->assertXpath('//textarea[@name="template[value]" and .=\'<div class="snippet-test">{{ 3 + 4 }}</div>\']');
    $this->assertXpath('//select/option[@selected="selected" and .="Snippet manager test basic format"]');

    $this->drupalGet($this->snippetUrl);
    $this->assertXpath('//div[@class="snippet-test" and .="7"]');
  }

}
