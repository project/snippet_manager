<?php

namespace Drupal\Tests\snippet_manager\Functional;

use Drupal\Component\Render\FormattableMarkup as FM;

/**
 * Snippet status test.
 *
 * @group snippet_manager
 */
class EnableDisableTest extends TestBase {

  /**
   * Tests snippet status actions.
   */
  public function testDefaultContext() {
    $this->drupalGet('admin/structure/snippet');
    $this->assertXpath('//td[.="alpha"]/../td[3][.="Enabled"]');

    $this->click('//td[.="alpha"]/../td//li/a[.="Disable"]');
    $this->assertXpath('//td[.="alpha"]/../td[3][.="Disabled"]');
    $this->assertStatusMessage(new FM('Snippet %label has been disabled.', ['%label' => 'Alpha']));

    $this->click('//td[.="alpha"]/../td//li/a[.="Enable"]');
    $this->assertXpath('//td[.="alpha"]/../td[3][.="Enabled"]');
  }

}
