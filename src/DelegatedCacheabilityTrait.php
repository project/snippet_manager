<?php

namespace Drupal\snippet_manager;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;

trait DelegatedCacheabilityTrait {

  /**
   * Get cacheanility.
   *
   * @return \Drupal\Core\Cache\CacheableDependencyInterface|null
   *   An object that carries this object's result cacheability.
   */
  abstract protected function cacheability(): ?CacheableDependencyInterface;

  /**
   * The cache contexts associated with this object.
   *
   * @return string[]
   *   An array of cache context tokens, used to generate a cache ID.
   */
  public function getCacheContexts() {
    $cacheability = $this->cacheability();
    return $cacheability ? $cacheability->getCacheContexts() : [];
  }

  /**
   * The cache tags associated with this object.
   *
   * @return string[]
   *   A set of cache tags.
   */
  public function getCacheTags() {
    $cacheability = $this->cacheability();
    return $cacheability ? $cacheability->getCacheTags() : [];
  }

  /**
   * The maximum age for which this object may be cached.
   *
   * @return int
   *   The maximum time in seconds that this object may be cached.
   */
  public function getCacheMaxAge() {
    $cacheability = $this->cacheability();
    return $cacheability ? $cacheability->getCacheMaxAge() : Cache::PERMANENT;
  }

}
